const express = require('express');
const Transaction = express.Router();
const TransactionModel = require('../Model/TransactionModel');
const STRIPE_SECRET_KEY = process.env.STRIPE_SECRET_KEY;

// Route for inserting payment transaction data
Transaction.post('/', async (req, res) => {
    try {
        const transactionData = {
            "TransactionId": req.body.TransactionId,
            "Name": req.body.Name,
            "Amount": req.body.Amount
        }

        const result = await TransactionModel.create(transactionData);
        res.send("Data Inserted Successfully");
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

// Route for retrieving all transactions
Transaction.get("/", async (req, res) => {
    try {
        const transactions = await TransactionModel.find({});

        if (transactions.length > 0) {
            res.send(transactions);
        } else {
            res.send("No Data Found");
        }
    } catch (errors) {
        console.error(errors);
        res.status(500).send("Internal Server Error");
    }
});



 

module.exports = Transaction;