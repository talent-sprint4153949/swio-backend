const express = require('express');
const app = express();
require("dotenv").config();
const {MONGO_URL}=process.env;
const {PORT} = process.env;
const mongoose = require('mongoose');
const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY)

const cors = require('cors');

app.use(express.json());
app.use(cors());

mongoose
    .connect(MONGO_URL,{

    })
   .then(()=>console.log("Mongoose Connected Successfully"))
   .catch((err)=>console.error(err));

app.use('/payment',require('./Routes/Transaction'));
app.use('/transc',require('./Routes/Transaction'))

app.post("/api/create-checkout-session", async (req, res) => {
    try {
        const { Transactions } = req.body; // Corrected the variable name

        // Mapping over Transactions array
        const lineItems = Transactions.map((transaction) => ({
            price_data: {
                currency: "inr",
                product_data: {
                    name: transaction.Name,
                },
                unit_amount: transaction.Price * 100,
            },
            quantity: 1, 
        }));

        const session = await stripe.checkout.sessions.create({
            payment_method_types: ["card"],
            mode: "payment",
            success_url: "http://localhost:3000/",
            cancel_url: "http://localhost:3000/cancel",
            line_items: lineItems, 
        });

        res.json({ id: session.id });
    } catch (error) {
        console.error("Error creating checkout session:", error);
        res.status(500).json({ error: "Server error creating checkout session" });
    }
});


app.listen(PORT,()=>{
    console.log(`Server is running on port ${PORT}`);
})

