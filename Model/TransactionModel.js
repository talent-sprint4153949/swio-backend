const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
    TransactionId:{
      type:String,
      required:true
    },
   
    Name: {
        type: String,
        required: true
    },
    Amount:{
        type:String,
        required:true
    }
})

const TransactionModel = mongoose.model('transactions',transactionSchema);

module.exports= TransactionModel;